require.config({
	paths: {
		jQuery: 'libs/jquery/jquery',
		Underscore: 'libs/underscore/underscore',
		Backbone: 'libs/backbone/backbone',
		jQueryMobile: 'libs/jquery-mobile/jquery-mobile',
		templates: '../templates'
	}

});

require(['app'], function(App){
	App.initialize();
});
