// Filename: router.js
define([
	'jQuery',
	'Underscore',
	'Backbone'
], function($, _, Backbone){
	var AppRouter = Backbone.Router.extend({

		routes: {
			// Define some URL routes
			'*actions': 'default'
		},
		default: function(actions){
			console.log('init');
		}
	});

	var initialize = function(){

		var app_router = new AppRouter;
		Backbone.history.start();

	};

	return { 
		initialize: initialize
	};
});
